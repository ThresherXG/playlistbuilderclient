import * as cookie from "cookie";
import { verifyUser } from "../utils/playlistBuilderApi";
import DataTable from "../components/DataTable";
import SideBar from "../components/SideBar";
import styles from "../styles/home.module.css";
import Builder from "../components/Builder";
import Title from "../components/Title";
import React, { useState } from "react";
import SelectPlaylist from "../components/SelectPlaylist";

const Home = (props: any) => {
  const [selectedPlaylist, setSelectedPlaylist] = useState();

  const homeComponent = selectedPlaylist ? (
    <>
      <div className={styles.playlistTitle}>
        <Title playlist={selectedPlaylist} />
      </div>
      <Builder playlist={selectedPlaylist} />
      <div className={styles.dataTableContainer}>
        <DataTable playlist={selectedPlaylist} />
      </div>
    </>
  ) : (
    <SelectPlaylist />
  );

  return (
    <div className={styles.mainContainer}>
      <div className={styles.leftContainer}>
        {props.sideNav}
        <SideBar
          setCurrentPlaylist={setSelectedPlaylist}
          currentPlaylist={selectedPlaylist}
        />
      </div>
      <div className={styles.rightContainer}>{homeComponent}</div>
    </div>
  );
};

export default Home;

//export async function getServerSideProps({ res, req }: any) {
//  if (!req.headers.cookie) {
//    return {
//      redirect: {
//        destination: "/account/login",
//        permanent: false,
//      },
//    };
//  } else {
//    const parsedCookies = cookie.parse(req.headers.cookie);
//
//    const tokenStatus = await verifyUser();
//
//    if (!tokenStatus) {
//      return {
//        redirect: {
//          destination: "/account/login",
//          permanent: false,
//        },
//      };
//    }
//  }
//
//  return {
//    props: {}, // will be passed to the page component as props
//  };
//}

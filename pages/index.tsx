import { Button, Typography } from "@material-ui/core";
import Link from "next/link";
import styles from "../styles/index.module.css";
import spotifyLogo from "../public/images//Spotify_Logo_RGB_Green_index.png";
import builderImg from "../public/images/playlist_builder_index.png";
import Logo from "../public/images/MusicPlaylistBuilder_index.png";
import Image from "next/image";

const Index = () => {
  return (
    <>
      <div className={styles.indexContainer}>
        <div className={styles.titleContainer}>
          <div className={styles.titleImgContainer}>
            <Image src={Logo} width={750} height={468} alt="Landing Logo" />
          </div>
          <Typography variant="h2" style={{ fontWeight: "bold" }}>
            Build customized playlists.
          </Typography>
          <Typography variant="h5" style={{ fontWeight: "bold" }}>
            Use various sources to import songs and playlists.
          </Typography>
          <Typography variant="h6">
            Ready to start? Create an account.
          </Typography>
          <div className={styles.registerButton}>
            <Link href="/account/register">
              <Button
                variant="contained"
                color="primary"
                disableElevation
                size="large"
              >
                Register
              </Button>
            </Link>
          </div>
        </div>
        <div className={styles.contentContainer}>
          <div className={styles.cardContentContainer}>
            <div className={styles.textContainer}>
              <Typography variant="h3">
                Filter, remove and add songs to build a customized playlist.
              </Typography>
            </div>
            <div className={styles.builderImgContainer}>
              <Image src={builderImg} width={550} height={253} alt="Home SS" />
            </div>
          </div>
        </div>
        <div className={styles.contentContainer}>
          <div className={styles.cardContentContainer}>
            <div className={styles.textContainer}>
              <Typography variant="h3">
                Upload playlists to Spotify to enjoy listening anywhere.
              </Typography>
            </div>
            <div className={styles.spotifyLogo}>
              <Image
                src={spotifyLogo}
                width={475}
                height={143}
                alt="Spotify Logo"
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Index;

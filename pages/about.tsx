import React from "react";
import Typography from "@material-ui/core/Typography";
import styles from "../styles/about.module.css";

const About = () => {
  return (
    <>
      <div className={styles.mainContainer}>
        <div className={styles.cardContainer}>
          <div className={styles.aboutCardContainer}>
            <div className={styles.aboutCardTitle}>
              <Typography variant="h4" style={{ fontWeight: "bold" }}>
                What is Music Playlist Builder?
              </Typography>
            </div>
            <Typography variant="body1">
              Music Playlist Builder was developed in order to give users the
              the ability to easily create Spotify playlists. See below
              fequently asked questions on the different ways to import songs
              and begin creating. If you do not see a question answered or have
              an idea for an improvement or encounter a bug please use the
              contract form to let us know!
            </Typography>
          </div>
          <div className={styles.aboutCardContainer}>
            <div className={styles.aboutCardTitle}>
              <Typography variant="h4" style={{ fontWeight: "bold" }}>
                Spotify
              </Typography>
            </div>
            <div className={styles.questionContainer}>
              <Typography variant="body1">
                To import playlists, albums and songs and also upload your
                playlist to Spotify, you must link your Spotify Account with
                Music Playlist Builder. You may unlink your account at anytime.
              </Typography>
            </div>
            <div className={styles.questionContainer}>
              <Typography variant="body1">
                Use a Spotify playlist, album or song urls to import songs to
                your custom playlist. Make sure it is the full url that is
                displayed in your browser.
              </Typography>
            </div>
            <div className={styles.questionContainer}>
              <Typography variant="body1">
                Once you have created your playlist exactly how you like it,
                upload it to Spotify using the Upload to Spotify Button.
              </Typography>
            </div>
          </div>
          <div className={styles.aboutCardContainer}>
            <div className={styles.aboutCardTitle}>
              <Typography variant="h4" style={{ fontWeight: "bold" }}>
                Pandora
              </Typography>
            </div>
            <div className={styles.questionContainer}>
              <Typography variant="body1">
                Use pandora playlist urls to import songs to your custom
                playlist. Make sure it is the full url that is displayed in your
                browser.
              </Typography>
            </div>
            <div className={styles.questionContainer}>
              <Typography variant="body1">
                I recieved an Pandora Authentication error? Wait a few minutes
                and then try to import your playlist from Pandora again.
              </Typography>
            </div>
          </div>
          <div className={styles.aboutCardContainer}>
            <div className={styles.aboutCardTitle}>
              <Typography variant="h4" style={{ fontWeight: "bold" }}>
                Coming soon
              </Typography>
              <div className={styles.questionContainer}>
                <Typography variant="body1">
                  Import playlists from SoundCloud.
                </Typography>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default About;

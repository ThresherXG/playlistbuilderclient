import React, { useEffect } from "react";
import Head from "next/head";
import CssBaseline from "@material-ui/core/CssBaseline";
import { ThemeProvider } from "@material-ui/core/styles";
import theme from "../utils/theme";
import Layout from "../components/Layout";
import Authenticator from "../components/Authenticator";
import "../styles/app.css";
import { QueryClient, QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      refetchOnMount: false,
      refetchOnReconnect: false,
      retry: false,
    },
  },
});

const MyApp = ({ Component, pageProps }: any) => {
  useEffect(() => {
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentElement?.removeChild(jssStyles);
    }
  }, []);

  return (
    <>
      <QueryClientProvider client={queryClient}>
        <Head>
          <title>MusicPlaylistBuilder</title>
          <meta
            name="viewport"
            content="viewport-fit=cover"
            initial-scale="1.0"
          />
          <meta
            name="description"
            content="Music Playlist Builder allows users to import playlists, albums and songs from various sources to build a custom playlist and upload the playlist to Spotify for easy listening anywhere."
          />
          <link rel="shortcut icon" href="/images/favicon.ico" />
          <link
            rel="apple-touch-icon"
            sizes="180x180"
            href="/images/favicon.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="32x32"
            href="/images/favicon.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="16x16"
            href="/images/favicon.png"
          />
        </Head>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <Authenticator>
            <Layout>
              <Component {...pageProps}></Component>
            </Layout>
          </Authenticator>
        </ThemeProvider>
        <ReactQueryDevtools initialIsOpen={false} />
      </QueryClientProvider>
    </>
  );
};

export default MyApp;

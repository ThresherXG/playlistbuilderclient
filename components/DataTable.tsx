/* eslint-disable react/display-name */
// @ts-ignore
import Typography from "@material-ui/core/Typography";
import styles from "../styles/datatable.module.css";
import { Itracks } from "../utils/interfaces";
import { Link } from "@material-ui/core";
import MUIDataTable from "mui-datatables";
import { debounceSearchRender } from "mui-datatables";
import Loader from "./Loader";
import { useQueryClient, useQuery } from "react-query";
import { getPlaylistTracks, deleteTracks } from "../utils/playlistBuilderApi";
import Error from "../components/Error";

const DataTable = (props: any) => {
  const queryClient = useQueryClient();

  const handleDeleteTracks = async (tracksToBeDeleted: Array<Itracks>) => {
    const response = await deleteTracks(tracksToBeDeleted);

    if (response.ok) {
      queryClient.invalidateQueries(props.playlist.playlist_id);
    }
  };

  const {
    data: playlistTracks,
    error,
    isError,
    isLoading,
  } = useQuery(props.playlist.playlist_id, () =>
    getPlaylistTracks(props.playlist.playlist_id, props.playlist.playlist_name)
  );

  const columns = [
    {
      name: "title",
      label: "Title",
      options: {
        filter: false,
        sort: true,
      },
    },
    {
      name: "album",
      label: "Album",
      options: {
        filter: false,
        sort: true,
      },
    },
    {
      name: "artist",
      label: "Artist",
      options: {
        filter: false,
        sort: true,
      },
    },
    {
      name: "source",
      label: "Source",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "source_name",
      label: "Imported From",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "source_url",
      label: "Link to Source",
      options: {
        filter: false,
        sort: true,
        customBodyRender: (source_url: string) =>
          source_url ? (
            <Link href={`${source_url}`} target="_blank" rel="noreferrer">
              Link
            </Link>
          ) : (
            ""
          ),
      },
    },
  ];

  const options: any = {
    filterType: "checkbox",
    responsive: "scrollFullHeight",
    print: false,
    fixedHeader: true,
    fixedSelectColumn: true,
    rowsPerPageOptions: [25, 50, 100],
    resizableColumns: true,
    searchPlaceholder: "Artists, Songs, or sources",
    customSearchRender: debounceSearchRender(500),
    onRowsDelete: async (e: any) => {
      var tracksToDelete = new Array();
      e.data.map((track: any) => {
        tracksToDelete.push(playlistTracks[track.dataIndex]);
      });

      await handleDeleteTracks(tracksToDelete);
    },
    elevation: 0,
  };

  if (isLoading) {
    return (
      <>
        <Loader />
      </>
    );
  }

  if (isError) {
    return (
      <>
        <Error />
      </>
    );
  }

  if (playlistTracks.length === 0) {
    return (
      <>
        <div className={styles.noTracks}>
          <Typography variant="h4">Playlist contains no songs</Typography>
        </div>
      </>
    );
  }

  return (
    <>
      <MUIDataTable
        title={""}
        data={playlistTracks}
        columns={columns}
        options={options}
      />
    </>
  );
};

export default DataTable;

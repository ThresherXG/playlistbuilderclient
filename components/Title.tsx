// @ts-ignore
import { createStyles, makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import { useEffect, useState } from "react";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import styles from "../styles/title.module.css";
import { useMutation, useQueryClient } from "react-query";
import { updatePlaylist, deletePlaylist } from "../utils/playlistBuilderApi";
import Tooltip from "@material-ui/core/Tooltip";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import CircularProgress from "@material-ui/core/CircularProgress";
import CheckIcon from "@material-ui/icons/CheckCircle";
import ErrorIcon from "@material-ui/icons/Error";
import { uploadSpotifyPlaylist } from "../utils/playlistBuilderApi";

const useStyles = makeStyles(
  createStyles({
    name: {
      "font-size": "50px",
      whiteSpace: "nowrap",
      overflow: "hidden",
      maxWidth: "35rem",
      textOverflow: "ellipsis",
    },
  })
);

const Title = (props: any) => {
  const classes = useStyles(props);
  const [name, setName] = useState(String);
  const [isNameFocused, setIsNamedFocused] = useState(false);
  const [playlistLoaded, setplaylistLoaded] = useState(false);
  const [playlistLoadedError, setplaylistLoadedError] = useState(false);
  const [playlistLoadedErrorMsg, setplaylistLoadedErrorMsg] = useState(String);
  const [isError, setError] = useState(false);
  const [errorText, setErrorText] = useState(String);
  const queryClient = useQueryClient();

  const handleUploadSpotifyPlaylist = async ({ playlist_id }: any) => {
    const response = await uploadSpotifyPlaylist({ playlist_id });

    if (response.ok) {
      setplaylistLoaded(true);
    } else {
      var msg = await response.json();

      setplaylistLoadedError(true);
      setplaylistLoadedErrorMsg(msg);
    }
  };

  const createSpotifyPlaylistMutation = useMutation(
    (spotifyPlaylistToCreate) =>
      handleUploadSpotifyPlaylist(spotifyPlaylistToCreate),
    {
      onMutate: () => {
        setplaylistLoadedError(false);
        setplaylistLoadedErrorMsg("");
      },

      onSettled: () => {
        queryClient.invalidateQueries("playlists");
      },
    }
  );

  const updateMutation = useMutation(
    (playlistToUpdate) => updatePlaylist(playlistToUpdate),
    {
      onMutate: async (playlistToUpdate: any) => {
        await queryClient.cancelQueries("playlists");

        const previousPlaylist = queryClient.getQueryData([
          "playlists",
          playlistToUpdate.playlist_id,
        ]);

        return { previousPlaylist, playlistToUpdate };
      },

      onSettled: () => {
        queryClient.invalidateQueries("playlists");
      },
    }
  );

  const deleteMutation = useMutation(
    (playlistToDelete) => deletePlaylist(playlistToDelete),
    {
      onMutate: () => {
        props.playlist.currentPlaylist.setCurrentPlaylist();
      },

      onSuccess: () => {
        props.playlist.currentPlaylist.setCurrentPlaylist();
      },

      onSettled: () => {
        queryClient.invalidateQueries("playlists");
      },
    }
  );

  const updatePlaylistFromInput = async (newName: string) => {
    setError(false);
    setErrorText("");
    if (name != props.playlist.playlist_name && !isError) {
      updateMutation.mutate({
        playlist_id: props.playlist.playlist_id,
        playlist_name: newName,
        spotify_created: props.playlist.spotify_created,
      });

      setName(newName);
    } else {
      setName(props.playlist.playlist_name);
    }
    setIsNamedFocused(false);
  };

  const handleDeleteClick = () => {
    if (!createSpotifyPlaylistMutation.isLoading) {
      deleteMutation.mutate({
        playlist_id: props.playlist.playlist_id,
        playlist_name: props.playlist.playlist_name,
      } as any);
    }
  };

  const handleOffEditTextFieldClick = (inputValue: string) => {
    if (!createSpotifyPlaylistMutation.isLoading) {
      updatePlaylistFromInput(inputValue);
    }
  };

  const handleEditClick = () => {
    setIsNamedFocused(true);
  };

  const titleError = (playlistTitle: string) => {
    if (playlistTitle === "") {
      setError(true);
      setErrorText("Invalid playlist title");
    } else {
      setError(false);
      setErrorText("");
    }

    setName(playlistTitle);
  };

  useEffect(() => {
    setName(props.playlist.playlist_name);
    setplaylistLoaded(props.playlist.spotify_created);
    setplaylistLoadedError(false);
  }, [props]);

  return (
    <>
      <Typography variant="body2">Playlist</Typography>
      {!isNameFocused ? (
        <Typography className={classes.name}>
          {name ? name : props.playlist.playlist_name}
        </Typography>
      ) : (
        <TextField
          error={isError}
          autoFocus
          id="standard-error-helper-text"
          helperText={errorText}
          inputProps={{ className: classes.name }}
          InputProps={{ disableUnderline: true }}
          value={name}
          onChange={(e) => titleError(e.target.value)}
          onBlur={(e) => handleOffEditTextFieldClick(e.target.value)}
        />
      )}
      <div className={styles.iconContainer}>
        <Tooltip title="Edit">
          <div className={styles.Icon} onClick={() => handleEditClick()}>
            <EditIcon />
          </div>
        </Tooltip>
        <Tooltip title="Delete">
          <div className={styles.Icon} onClick={() => handleDeleteClick()}>
            <DeleteIcon />
          </div>
        </Tooltip>
        {createSpotifyPlaylistMutation.isLoading ? (
          <div className={styles.loadingIcon}>
            <CircularProgress
              size="2rem"
              style={{ alignItems: "baseline" }}
              color="secondary"
            />
          </div>
        ) : (
          <Tooltip title="Upload to Spotify">
            <div
              className={styles.spotifyIcon}
              onClick={() =>
                createSpotifyPlaylistMutation.mutate({
                  playlist_id: props.playlist.playlist_id,
                } as any)
              }
              title="Upload to Spotify"
            >
              <CloudUploadIcon />
            </div>
          </Tooltip>
        )}
        {playlistLoaded ? (
          <Tooltip title="Playlist successfully uploaded to Spotify">
            <div className={styles.spotifyLoadedIcon}>
              <CheckIcon />
            </div>
          </Tooltip>
        ) : (
          <></>
        )}
        {playlistLoadedError ? (
          <Tooltip
            title={
              playlistLoadedErrorMsg
                ? playlistLoadedErrorMsg
                : "An error has occured please try again later"
            }
          >
            <div className={styles.errorIcon}>
              <ErrorIcon />
            </div>
          </Tooltip>
        ) : (
          <></>
        )}
      </div>
    </>
  );
};

export default Title;

import { useRouter } from "next/router";
import { Button } from "@material-ui/core";
import styles from "../styles/logobar.module.css";
import Link from "next/link";
import Image from "next/image";
import Logo from "../public/images/MusicPlaylistBuilder_logo.png";

const LogoBar = () => {
  let router = useRouter();

  let signOnButton = <></>;

  if (router.pathname === "/") {
    signOnButton = (
      <Button
        variant="contained"
        color="primary"
        size="medium"
        onClick={() => router.push("/account/login")}
        disableElevation
      >
        Sign In
      </Button>
    );
  }

  return (
    <>
      <div className={styles.logoBarContainer}>
        <div className={styles.mainContainer}>
          <div className={styles.logoContainer}>
            <Link href="/">
              <Image
                src={Logo}
                width={75}
                height={47}
                layout="fixed"
                alt="Logo"
                className={styles.logo}
              />
            </Link>
          </div>
          <div className={styles.signInButtonContainer}>{signOnButton}</div>
        </div>
      </div>
    </>
  );
};

export default LogoBar;

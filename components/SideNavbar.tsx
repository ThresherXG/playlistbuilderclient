import MatLink from "@material-ui/core/Link";
import Link from "next/link";
import Typography from "@material-ui/core/Typography";
import { Button } from "@material-ui/core";
import styles from "../styles/sidenavbar.module.css";
import React, { useState, useEffect } from "react";
import Image from "next/image";
import spotifyIcon from "../public/images/Spotify_Icon_RGB_Green_sideNav.png";
import router from "next/router";
import { useQueryClient } from "react-query";
import { IuserProfile } from "../utils/interfaces";
import { spotifyLink, spotifyUnLink } from "../utils/playlistBuilderApi";
import Logo from "../public/images/MusicPlaylistBuilder_logo.png";
import Cookies from "js-cookie";

const SideNavbar = () => {
  const [spotifyLinked, setSpotifyLinked] = useState("Connect");
  const [buttonState, setButtonState] = useState(false);
  const queryClient = useQueryClient();
  const [userProfile, setUserProfile] = useState<any>(
    queryClient.getQueryData("user") as IuserProfile
  );

  const spotifyButtonState = () => {
    if (userProfile?.spotify_link === true) {
      setSpotifyLinked("Disconnet");
    } else {
      setSpotifyLinked("Connect");
    }
  };

  const handleSpotifyLink = async () => {
    if (spotifyLinked === "Connect") {
      const response = await spotifyLink();

      const spotifyAuthWindow = await response.text();

      opener = () => {
        const myWindow = window.open(
          spotifyAuthWindow,
          "Spotify Link",
          "width=650,height=2500"
        );

        setButtonState(true);
        var timer = setInterval(async function () {
          if (myWindow?.closed) {
            clearInterval(timer);
            setButtonState(false);
            await queryClient.refetchQueries("user");
            setUserProfile(queryClient.getQueryData("user"));
          }
        }, 50);
      };

      opener();
    } else {
      const response = await spotifyUnLink();

      if (response.ok) {
        await queryClient.refetchQueries("user");
        setUserProfile(queryClient.getQueryData("user"));
      }
    }
  };

  const signOut = () => {
    Cookies.remove("auth-token", { path: "/" });
    router.push("/account/login");
  };

  const spotifyUserNameDiv =
    spotifyLinked === "Disconnet" ? (
      <MatLink
        href={userProfile?.spotify_account_url}
        variant="h6"
        target="_blank"
        rel="noreferrer"
        color="inherit"
      >
        {userProfile?.spotify_display_name}
      </MatLink>
    ) : (
      <Typography variant="h6">Connect</Typography>
    );

  useEffect(() => {
    spotifyButtonState();
  }, [userProfile]);

  useEffect(() => {
    setUserProfile(queryClient.getQueryData("user"));
  }, [queryClient.getQueryData("user")]);

  return (
    <div className={styles.navContainer}>
      <div className={styles.mainContainer}>
        <div className={styles.topContainer}>
          <div className={styles.logoContainer}>
            <Link href="/home">
              <Image src={Logo} width={75} height={47} alt="Logo" />
            </Link>
          </div>
          <div className={styles.aboutContainer}>
            <Link href="/about">
              <Typography variant="h6" className={styles.aboutLink}>
                About
              </Typography>
            </Link>
          </div>
          <div className={styles.contactContainer}>
            <Link href="/contact">
              <Typography variant="h6" className={styles.contactLink}>
                Contact
              </Typography>
            </Link>
            <div></div>
          </div>
        </div>
        <div className={styles.spotifyIcon}>
          <MatLink
            href={"https://open.spotify.com/"}
            target="_blank"
            rel="noreferrer"
            color="inherit"
          >
            <Image
              src={spotifyIcon}
              width={60}
              height={60}
              alt="Spotify Icon"
            />
          </MatLink>

          <div className={styles.spotifyProfileContainer}>
            <div className={styles.spotifyProfileNameContainer}>
              {spotifyUserNameDiv}
              <div className={styles.spotifyLinkButtonContainer}>
                <Button
                  disabled={buttonState}
                  variant="contained"
                  color="secondary"
                  size="small"
                  onClick={() => handleSpotifyLink()}
                  disableElevation
                >
                  {spotifyLinked}
                </Button>
              </div>
            </div>
          </div>
        </div>
        <div className={styles.profileContainer}>
          <div className={styles.profileNameContainer}>
            <div className={styles.profileName}>
              <Typography
                variant="h6"
                style={{
                  textOverflow: "ellipsis",
                  whiteSpace: "nowrap",
                  overflow: "hidden",
                }}
              >
                {userProfile?.user_name}
              </Typography>
            </div>
          </div>
          <div className={styles.profileButtonContainer}>
            <Button
              variant="contained"
              color="primary"
              size="small"
              onClick={() => signOut()}
              disableElevation
            >
              Sign Out
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SideNavbar;

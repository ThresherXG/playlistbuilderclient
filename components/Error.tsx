import Tooltip from "@material-ui/core/Tooltip";
import ErrorIcon from "@material-ui/icons/Error";
import styles from "../styles/error.module.css";

const Error = () => {
  return (
    <>
      <div>
        <Tooltip title="An error has occured please try agian later">
          <div className={styles.errorContainer}>
            <ErrorIcon />
          </div>
        </Tooltip>
      </div>
    </>
  );
};

export default Error;

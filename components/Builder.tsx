import {
  Button,
  FormControl,
  Input,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@material-ui/core";
import styles from "../styles/builder.module.css";
import CloudDownloadIcon from "@material-ui/icons/CloudDownload";
import CircularProgress from "@material-ui/core/CircularProgress";
import { useState, useEffect } from "react";
import Cookies from "js-cookie";
import { useMutation, useQueryClient } from "react-query";

const Builder = (props: any) => {
  const [buttonState, setButtonState] = useState(true);
  const [selectState, setSelectState] = useState(String);
  const [inputState, setInputState] = useState(String);
  const [inputError, setInputErrorState] = useState(false);
  const [inputErrorText, setInputErrorText] = useState(String);
  const queryClient = useQueryClient();

  const handleButtonState = (e: any) => {
    if (inputState != "" && selectState != "") {
      setButtonState(false);
    } else {
      setButtonState(true);
    }
  };

  const getRouteConstructPayload = (select: string, userInput: string) => {
    let route = null;
    let payload = null;

    switch (select) {
      case "Itunes File":
        route =
          "https://musicplaylistbuilder.azurewebsites.net/api/itunes/upload/" +
          props.playlist.playlist_id;
        break;
      case "Pandora Station":
        route =
          "https://musicplaylistbuilder.azurewebsites.net/api/pandora/station";
        payload = {
          playlist_id: props.playlist.playlist_id,
          pandora_station_url: userInput,
          pandora_playlist_url: "",
        };
        break;
      case "Pandora Playlist":
        route =
          "https://musicplaylistbuilder.azurewebsites.net/api/pandora/playlist";
        payload = {
          playlist_id: props.playlist.playlist_id,
          pandora_station_url: "",
          pandora_playlist_url: userInput,
        };
        break;
      case "Spotify Playlist":
        route =
          "https://musicplaylistbuilder.azurewebsites.net/api/spotify/playlist";
        payload = {
          playlist_id: props.playlist.playlist_id,
          spotify_playlist_url: userInput,
          spotify_album_url: "",
          spotify_track_url: "",
        };
        break;
      case "Spotify Album":
        route =
          "https://musicplaylistbuilder.azurewebsites.net/api/spotify/album";
        payload = {
          playlist_id: props.playlist.playlist_id,
          spotify_playlist_url: "",
          spotify_album_url: userInput,
          spotify_track_url: "",
        };
        break;
      case "Spotify Song":
        route =
          "https://musicplaylistbuilder.azurewebsites.net/api/spotify/track";
        payload = {
          playlist_id: props.playlist.playlist_id,
          spotify_playlist_url: "",
          spotify_album_url: "",
          spotify_track_url: userInput,
        };
        break;
    }

    return { route, payload };
  };

  const uploadTracks = async () => {
    const routeAndPayload: any = getRouteConstructPayload(
      selectState,
      inputState
    );

    if (selectState != "iTunes File") {
      const response = await fetch(routeAndPayload.route, {
        method: "POST",
        headers: {
          Authorization: "Bearer " + Cookies.get("auth-token"),
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(routeAndPayload.payload),
      });

      const returnMessage = await response.json();

      if (response.status != 200) {
        setInputErrorState(true);
        if (returnMessage) {
          setInputErrorText(returnMessage);
        } else {
          setInputErrorText("An unexpected Error as occured, please try again");
        }
      } else {
        setInputErrorState(false);
        setInputErrorText("");
      }
    }
  };

  const uploadTracksMutation = useMutation(uploadTracks, {
    onMutate: () => {
      setInputErrorState(false);
      setInputErrorText("");
    },

    onSettled: () => {
      queryClient.invalidateQueries(props.playlist.playlist_id);
    },
  });

  useEffect(() => {
    setInputErrorState(false);
    setInputErrorText("");
  }, [props]);

  return (
    <>
      <div className={styles.builderContainer}>
        <FormControl variant="outlined">
          <InputLabel id="demo-simple-select-outlined-label">Select</InputLabel>
          <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            value={selectState}
            onChange={(e: any) => setSelectState(e.target.value)}
            onBlur={handleButtonState}
            label="Select"
            style={{ width: "15rem" }}
          >
            <MenuItem value="">
              <em>Select</em>
            </MenuItem>
            <MenuItem value={"Pandora Playlist"}>Pandora Playlist Url</MenuItem>
            <MenuItem value={"Spotify Playlist"}>Spotify Playlist Url</MenuItem>
            <MenuItem value={"Spotify Album"}>Spotify Album Url</MenuItem>
            <MenuItem value={"Spotify Song"}>Spotify Song Url</MenuItem>
          </Select>
        </FormControl>
        <TextField
          error={inputError}
          id="outlined-error-helper-text"
          label="Paste URL Here"
          placeholder="example: https://open.spotify.com/playlist/37i9dQZF1DXcBWIGoYBM5M"
          variant="outlined"
          margin="normal"
          style={{ width: "50%" }}
          helperText={inputErrorText}
          onChange={(e) => setInputState(e.target.value)}
          onBlur={handleButtonState}
        />
        <div className={styles.uploadLoader}>
          {uploadTracksMutation.isLoading ? (
            <CircularProgress />
          ) : (
            <Button
              disabled={buttonState}
              variant="contained"
              color="primary"
              startIcon={<CloudDownloadIcon />}
              disableElevation
              onClick={() => uploadTracksMutation.mutate()}
            >
              Import
            </Button>
          )}
        </div>
      </div>
    </>
  );
};

export default Builder;

//<input
//accept=".txt"
//style={{ display: "none" }}
//id="contained-button-file"
//type="file"
///>
//<label htmlFor="contained-button-file">
//<Button
//  variant="contained"
//  color="primary"
//  component="span"
//  disableElevation
//>
//  Upload
//</Button>
//</label>

import Link from "next/link";
import { useState } from "react";
import { TextField, Button, FormControl } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import styles from "../styles/registerform.module.css";
import { useEffect } from "react";
import Cookies from "js-cookie";
import router from "next/router";
import { useQueryClient, useMutation } from "react-query";
import { registerUser } from "../utils/playlistBuilderApi";
import CircularProgress from "@material-ui/core/CircularProgress";

const RegisterForm = () => {
  const [error, setError] = useState("");
  const [userName, setUserName] = useState("");
  const [userEmail, setUserEmail] = useState("");
  const [userPassword, setUserPassword] = useState("");
  const [buttonState, setButtonState] = useState(true);
  const [emailError, setEmailError] = useState("");
  const [emailBool, setEmailBool] = useState(false);
  const [passwordError, setPasswordError] = useState("");
  const [passwordBool, setPasswordBool] = useState(false);
  const [userNameError, setUserNameError] = useState("");
  const [userNameBool, setUserNameBool] = useState(false);
  const queryClient = useQueryClient();

  const errorDiv = error ? (
    <Typography variant="body2" style={{ color: "red" }}>
      {error}
    </Typography>
  ) : null;

  const userInputState = (value: string) => {
    setUserName(value);
    setUserNameBool(false);
    setUserNameError("");
  };

  const emailInputState = (value: string) => {
    setUserEmail(value);
    setEmailBool(false);
    setEmailError("");
  };

  const passwordInputState = (value: string) => {
    setUserPassword(value);
    setPasswordBool(false);
    setPasswordError("");
  };

  const userNameErrorState = (userName: string) => {
    if (userName === "") {
      setUserNameBool(true);
      setUserNameError("User Name is required");
    }
  };

  const emailErrorState = (userEmail: string) => {
    if (userEmail === "") {
      setEmailBool(true);
      setEmailError("Email Address is required");
    }
  };

  const passwordErrorState = (userPassword: string) => {
    if (userPassword === "") {
      setPasswordBool(true);
      setPasswordError("Password is required");
    }
  };

  const setButton = () => {
    if (userEmail === "" || userPassword === "" || userName === "") {
      setButtonState(true);
    } else {
      setButtonState(false);
    }
  };

  const handleRegisterUser = async (
    userName: string,
    userEmail: string,
    userPassword: string
  ) => {
    const response = await registerUser(userName, userEmail, userPassword);

    const data = await response.json();

    if (!response.ok) {
      setError(data);
    } else {
      Cookies.set("auth-token", data, { expires: 7 });
      await router.push("/home");
      await queryClient.refetchQueries("user");
    }
  };

  const registerMutation = useMutation(
    () => handleRegisterUser(userName, userEmail, userPassword),
    {
      onMutate: () => {},

      onError: () => {},

      onSettled: () => {},
    }
  );

  useEffect(() => {
    setButton();
  }, [userName, userEmail, userPassword]);

  return (
    <>
      <div className={styles.mainContainer}>
        <div className={styles.registerContainer}>
          <div className={styles.formContainer}>
            <Typography variant="h5" id="sign-in-title">
              Create an Account
            </Typography>
            {errorDiv}
            <FormControl>
              <TextField
                error={userNameBool}
                id="outlined-error-helper-text"
                label="User Name"
                variant="outlined"
                margin="normal"
                helperText={userNameError}
                onChange={(e) => userInputState(e.target.value)}
                onBlur={() => userNameErrorState(userName)}
              />
              <TextField
                error={emailBool}
                id="outlined-error-helper-text"
                label="Email Address"
                variant="outlined"
                margin="normal"
                helperText={emailError}
                onChange={(e) => emailInputState(e.target.value)}
                onBlur={() => emailErrorState(userEmail)}
              />
              <TextField
                error={passwordBool}
                type="password"
                id="outlined-error-helper-text"
                label="Password"
                variant="outlined"
                margin="normal"
                helperText={passwordError}
                onChange={(e) => passwordInputState(e.target.value)}
                onBlur={() => passwordErrorState(userPassword)}
              />
              {registerMutation.isLoading ? (
                <div className={styles.registerLoader}>
                  <CircularProgress />
                </div>
              ) : (
                <Button
                  id="sign-in-buttom"
                  variant="contained"
                  color="primary"
                  size="large"
                  disabled={buttonState}
                  onClick={() => registerMutation.mutate()}
                >
                  Register
                </Button>
              )}
            </FormControl>
            <div className={styles.existingUserContainer}>
              <Typography variant="body2">Have an account?</Typography>
              <Link href="/account/login">
                <Typography variant="body2">
                  <a className={styles.login}>Log In</a>
                </Typography>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default RegisterForm;

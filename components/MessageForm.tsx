import { Button, FormControl, TextField, Typography } from "@material-ui/core";
import { useMutation } from "react-query";
import { useState, useEffect } from "react";
import React from "react";
import styles from "../styles/messageform.module.css";
import CircularProgress from "@material-ui/core/CircularProgress";
import { sendMessage } from "../utils/playlistBuilderApi";

const MessageForm = () => {
  const [returnEmail, setReturnEmail] = useState("");
  const [subject, setSubject] = useState("");
  const [body, setBody] = useState("");
  const [subjectBool, setSubjectBool] = useState(false);
  const [bodyBool, setBodyBool] = useState(false);
  const [subjectError, setSubjectError] = useState("");
  const [bodyError, setBodyError] = useState("");
  const [buttonState, setButtonState] = useState(true);
  const [error, setError] = useState(Boolean);
  const [msgText, setmsgText] = useState(String);

  const messageDiv = error ? (
    <Typography variant="body2">{msgText}</Typography>
  ) : null;

  const setButton = () => {
    if (subject === "" || body === "") {
      setButtonState(true);
    } else {
      setButtonState(false);
    }
  };

  const subjectInputState = (value: string) => {
    setSubject(value);
    setSubjectBool(false);
    setSubjectError("");
  };

  const subjectErrorState = (subject: string) => {
    if (subject === "") {
      setSubjectBool(true);
      setSubjectError("Subject is required");
    }
  };

  const bodyInputState = (value: string) => {
    setBody(value);
    setBodyBool(false);
    setBodyError("");
  };

  const bodyErrorState = (body: string) => {
    if (body === "") {
      setBodyBool(true);
      setBodyError("Message body is required");
    }
  };

  const handleSendMessage = async (message: any) => {
    const response = await sendMessage(message);

    if (response.ok) {
      setError(true);
      setmsgText("Message has been sent successfully");
    } else {
      setError(true);
      setmsgText("There has been an error please try again");
    }
  };

  const sendMessageMutation = useMutation(
    (message: any) => handleSendMessage(message),
    {
      onMutate: () => {},

      onError: () => {},

      onSettled: () => {},
    }
  );

  useEffect(() => {
    setButton();
  }, [subject, body]);

  return (
    <>
      <div className={styles.mainContainer}>
        <div className={styles.titleContainer}>
          <Typography variant="h4" id="sign-in-title">
            Find a bug? Idea for an impovment? Would like a feature added?
          </Typography>
        </div>
        <div className={styles.sendMessageContainer}>
          <div className={styles.formContainer}>
            <Typography variant="h5" id="sign-in-title">
              Send Message
            </Typography>
            <FormControl>
              <TextField
                id="outlined-error-helper-text"
                label="Return Email (Optional)"
                variant="outlined"
                margin="normal"
                onChange={(e) => setReturnEmail(e.target.value)}
              />
              <TextField
                error={subjectBool}
                id="outlined-error-helper-text"
                label="Subject"
                variant="outlined"
                margin="normal"
                helperText={subjectError}
                onChange={(e) => subjectInputState(e.target.value)}
                onBlur={() => subjectErrorState(subject)}
              />
              <TextField
                error={bodyBool}
                id="outlined-multiline-static"
                label="Message"
                multiline
                rows={12}
                variant="outlined"
                helperText={bodyError}
                onChange={(e) => bodyInputState(e.target.value)}
                onBlur={() => bodyErrorState(body)}
              />
              {sendMessageMutation.isLoading ? (
                <div className={styles.loaderContainer}>
                  <CircularProgress />
                </div>
              ) : (
                <Button
                  id="sign-in-buttom"
                  variant="contained"
                  color="primary"
                  size="large"
                  style={{ marginTop: "1rem" }}
                  disabled={buttonState}
                  onClick={() =>
                    sendMessageMutation.mutate({
                      subject: subject,
                      reply_email: returnEmail,
                      body: body,
                    })
                  }
                >
                  Send Message
                </Button>
              )}
            </FormControl>
            <div className={styles.returnMsgContainer}>{messageDiv}</div>
          </div>
        </div>
      </div>
    </>
  );
};

export default MessageForm;

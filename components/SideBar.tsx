import { Button } from "@material-ui/core";
import styles from "../styles/sidebar.module.css";
import Playlist from "../components/Playlist";
import Typography from "@material-ui/core/Typography";
import { useEffect } from "react";
import { Iplaylist } from "../utils/interfaces";
import Loader from "./Loader";
import { useMutation, useQueryClient, useQuery } from "react-query";
import Error from "../components/Error";
import { createPlaylist, getPlaylists } from "../utils/playlistBuilderApi";
import router from "next/router";

const SideBar = (props: any) => {
  const queryClient = useQueryClient();

  const {
    data: playlists,
    error,
    isError,
    isLoading,
  } = useQuery("playlists", getPlaylists);

  const createMutation = useMutation(createPlaylist, {
    onMutate: async () => {
      await queryClient.cancelQueries("playlists");

      const previousPlaylists = queryClient.getQueryData("playlists");

      queryClient.setQueryData("playlists", (oldData: any) => [
        ...oldData,
        { playlist_id: Math.random(), playlist_name: "New Playlist" },
      ]);

      return { previousPlaylists };
    },

    onSettled: () => {
      queryClient.invalidateQueries("playlists");
    },
  });

  useEffect(() => {
    queryClient.invalidateQueries("playlists");
  }, [queryClient.getQueryData("user")]);

  if (isLoading) {
    return (
      <div>
        <Loader />
      </div>
    );
  }

  if (isError) {
    return (
      <>
        <Error />
      </>
    );
  }

  return (
    <>
      <div className={styles.sideNavContainer}>
        <div className={styles.titleContainer}>
          <Typography variant="h6" style={{ fontWeight: 600 }}>
            Playlists
          </Typography>
        </div>
        <div className={styles.playlistContainer}>
          {playlists?.map((playlist: Iplaylist) => (
            <Playlist
              key={playlist.playlist_id}
              playlist_id={playlist.playlist_id}
              playlist_name={playlist.playlist_name}
              spotify_created={playlist.spotify_created}
              currentPlaylist={props}
            />
          ))}
        </div>
      </div>
      <div className={styles.buttonContainer}>
        <Button
          variant="contained"
          color="primary"
          disableElevation
          onClick={() => createMutation.mutate(playlists)}
        >
          Create PlayList
        </Button>
      </div>
    </>
  );
};

export default SideBar;

import { Typography } from "@material-ui/core";
import { useState, useEffect } from "react";
import styles from "../styles/playlist.module.css";

const Playlist = (props: any) => {
  const [style, setStyle] = useState({});

  const handlePlaylistClick = () => {
    props.currentPlaylist.setCurrentPlaylist(props);
    setStyle({ fontWeight: 600 });
  };

  const handlePlaylistOffClick = () => {
    if (
      props.playlist_id != undefined &&
      props.currentPlaylist.currentPlaylist != undefined &&
      props.playlist_id != props.currentPlaylist.currentPlaylist.playlist_id
    ) {
      setStyle({});
    }
  };

  useEffect(() => {
    handlePlaylistOffClick();
  }, [props]);

  return (
    <>
      <div className={styles.mainContainer}>
        <div className={styles.playlistName}>
          <Typography
            variant="body2"
            className={styles.playlistTitle}
            style={style}
            onClick={() => handlePlaylistClick()}
          >
            {props.playlist_name}
          </Typography>
        </div>
      </div>
    </>
  );
};

export default Playlist;

import CircularProgress from "@material-ui/core/CircularProgress";
import styles from "../styles/loader.module.css";

const Loader = () => {
  return (
    <div className={styles.mainContainer}>
      <CircularProgress />
    </div>
  );
};

export default Loader;

import Navbar from "./Navbar";
import LogoBar from "./LogoBar";
import { useRouter } from "next/router";
import React from "react";
import styles from "../styles/layout.module.css";
import SideNavbar from "./SideNavbar";
import Loader from "./Loader";
import { useQuery, useQueryClient } from "react-query";
import Cookies from "js-cookie";
import { getUserProfile } from "../utils/playlistBuilderApi";

const Layout = ({ children }: any) => {
  const router = useRouter();
  const queryClient = useQueryClient();

  let navBar;

  const {
    data: userProfile,
    error,
    isError,
    isLoading,
    isFetching,
  } = useQuery("user", getUserProfile, {
    enabled: false,
  });

  if (!userProfile && Cookies.get("auth-token")) {
    queryClient.refetchQueries("user");
  }

  if (isLoading) {
    return <Loader />;
  }

  if (router.pathname === "/home") {
    return (
      <div className={styles.mainLayout}>
        {React.Children.map(children, (child) =>
          React.cloneElement(child, {
            sideNav: <SideNavbar />,
          })
        )}
      </div>
    );
  }

  if (router.pathname === "/contact" || router.pathname === "/about") {
    navBar = <Navbar />;
  } else {
    navBar = <LogoBar />;
  }

  return (
    <div className={styles.mainLayout}>
      {navBar}
      {React.Children.map(children, (child) => React.cloneElement(child))}
    </div>
  );
};

export default Layout;

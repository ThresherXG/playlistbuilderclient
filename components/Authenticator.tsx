import router from "next/router";
import Loader from "./Loader";
import { useEffect, useState } from "react";
import Cookies from "js-cookie";
import React from "react";
import { verifyUser } from "../utils/playlistBuilderApi";

const Authenticator = ({ children }: any) => {
  const [loader, setLoader] = useState(true);
  const [auth, setAuth] = useState(false);

  const authenticate = async () => {
    if (Cookies.get("auth-token")) {
      const isTokenValid: boolean = await verifyUser();

      if (!isTokenValid) {
        await router.push("/account/login");
        Cookies.remove("auth-token", { path: "/" });
        setAuth(false);
        setLoader(false);
      } else {
        if (
          router.pathname === "/account/login" ||
          router.pathname === "/account/register" ||
          router.pathname === "/"
        ) {
          await router.push("/home");
          setAuth(true);
          setLoader(false);
        } else {
          setAuth(true);
          setLoader(false);
        }
      }
    } else {
      if (router.pathname === "/account/register" || router.pathname === "/") {
        setAuth(false);
        setLoader(false);
      } else {
        await router.push("/account/login");
        setAuth(false);
        setLoader(false);
      }
    }
  };

  useEffect(() => {
    let isMounted = true;
    authenticate();
    return () => {
      isMounted = false;
    };
  }, []);

  while (loader) {
    return (
      <>
        <Loader />
      </>
    );
  }

  // {React.Children.map(children, (child) =>
  //React.cloneElement(child, { userAuth: auth })
  //)}
  return <>{children}</>;
};

export default Authenticator;

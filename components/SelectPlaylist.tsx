import Typography from "@material-ui/core/Typography";

const SelectPlaylist = () => {
  return (
    <>
      <Typography
        variant="h4"
        style={{ marginLeft: "2rem", marginTop: "2rem" }}
      >
        Select or Create a Playlist
      </Typography>
    </>
  );
};

export default SelectPlaylist;

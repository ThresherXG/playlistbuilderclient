import { useState } from "react";
import Link from "next/link";
import { TextField, Button, FormControl, Box } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import styles from "../styles/loginform.module.css";
import { useEffect } from "react";
import Cookies from "js-cookie";
import router from "next/router";
import { useQueryClient, useMutation } from "react-query";
import { signIn } from "../utils/playlistBuilderApi";
import CircularProgress from "@material-ui/core/CircularProgress";

const LoginForm = () => {
  const [userEmail, setUserEmail] = useState("");
  const [userPassword, setUserPassword] = useState("");
  const [error, setError] = useState("");
  const [buttonState, setButtonState] = useState(true);
  const [emailError, setEmailError] = useState("");
  const [emailBool, setEmailBool] = useState(false);
  const [passwordError, setPasswordError] = useState("");
  const [passwordBool, setPasswordBool] = useState(false);
  const queryClient = useQueryClient();

  const errorDiv = error ? (
    <Typography variant="body2" style={{ color: "red" }}>
      {error}
    </Typography>
  ) : null;

  const emailInputState = (value: string) => {
    setUserEmail(value);
    setEmailBool(false);
    setEmailError("");
  };

  const passwordInputState = (value: string) => {
    setUserPassword(value);
    setPasswordBool(false);
    setPasswordError("");
  };

  const emailErrorState = (userEmail: string) => {
    if (userEmail === "") {
      setEmailBool(true);
      setEmailError("Email Address is required");
    }
  };

  const passwordErrorState = (userPassword: string) => {
    if (userPassword === "") {
      setPasswordBool(true);
      setPasswordError("Password is required");
    }
  };

  const setButton = () => {
    if (userEmail === "" || userPassword === "") {
      setButtonState(true);
    } else {
      setButtonState(false);
    }
  };

  const handleSignIn = async (userEmail: string, userPassword: string) => {
    const response = await signIn(userEmail, userPassword);

    var data = await response.json();

    if (!response.ok) {
      setError(data);
    } else {
      Cookies.set("auth-token", data, { expires: 7 });
      await router.push("/home");
      await queryClient.refetchQueries("user");
    }
  };

  const loginMutation = useMutation(
    () => handleSignIn(userEmail, userPassword),
    {
      onMutate: () => {},

      onError: () => {},

      onSettled: () => {},
    }
  );

  useEffect(() => {
    setButton();
  }, [userEmail, userPassword]);

  return (
    <>
      <div className={styles.mainContainer}>
        <div className={styles.loginContainer}>
          <div className={styles.formContainer}>
            <Typography variant="h5" id="sign-in-title">
              Sign In
            </Typography>
            {errorDiv}
            <FormControl>
              <TextField
                error={emailBool}
                id="outlined-error-helper-text"
                label="Email Address"
                variant="outlined"
                margin="normal"
                helperText={emailError}
                onChange={(e) => emailInputState(e.target.value)}
                onBlur={() => emailErrorState(userEmail)}
              />
              <TextField
                error={passwordBool}
                type="password"
                id="outlined-error-helper-text"
                label="Password"
                variant="outlined"
                margin="normal"
                helperText={passwordError}
                onChange={(e) => passwordInputState(e.target.value)}
                onBlur={() => passwordErrorState(userPassword)}
              />
              {loginMutation.isLoading ? (
                <div className={styles.loginLoader}>
                  <CircularProgress />
                </div>
              ) : (
                <Button
                  id="sign-in-buttom"
                  variant="contained"
                  color="primary"
                  size="large"
                  disabled={buttonState}
                  onClick={() => loginMutation.mutate()}
                >
                  Sign In
                </Button>
              )}
            </FormControl>
            <div className={styles.newUserContainer}>
              <Typography variant="body2">New User?</Typography>
              <Link href="/account/register">
                <Typography variant="body2">
                  <a className={styles.signup} id="sign-up-bottom">
                    Sign Up
                  </a>
                </Typography>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default LoginForm;

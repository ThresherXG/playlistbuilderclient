import Link from "next/link";
import Typography from "@material-ui/core/Typography";
import { Button } from "@material-ui/core";
import styles from "../styles/navbar.module.css";
import React, { useState, useEffect } from "react";
import router from "next/router";
import { useQueryClient } from "react-query";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { IuserProfile } from "../utils/interfaces";
import Image from "next/image";
import Cookies from "js-cookie";
import Logo from "../public/images/MusicPlaylistBuilder_logo.png";

const Navbar = () => {
  const [userProfile, setUserProfile] = useState<IuserProfile>();
  const [anchorEl, setAnchorEl] = useState(null);
  const queryClient = useQueryClient();

  const signOut = () => {
    Cookies.remove("auth-token", { path: "/" });
    router.push("/account/login");
  };

  const options = [
    <Link href="/about" key="1">
      About
    </Link>,
    <Link href="/contact" key="2">
      Contact
    </Link>,
  ];

  const open = Boolean(anchorEl);

  const handleClick = (e: any) => {
    setAnchorEl(e.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const ITEM_HEIGHT = 48;

  useEffect(() => {
    setUserProfile(queryClient.getQueryData("user"));
  }, []);

  return (
    <div className={styles.navContainer}>
      <div className={styles.mainContainer}>
        <div className={styles.leftContainer}>
          <div className={styles.logoContainer}>
            <Link href="/home">
              <Image
                src={Logo}
                width={75}
                height={47}
                layout="fixed"
                alt="Logo"
              />
            </Link>
          </div>
          <div className={styles.aboutContainer}>
            <Link href="/about">
              <Typography variant="h6" className={styles.aboutLink}>
                About
              </Typography>
            </Link>
          </div>
          <div className={styles.contactContainer}>
            <Link href="/contact">
              <Typography variant="h6" className={styles.contactLink}>
                Contact
              </Typography>
            </Link>
            <div></div>
          </div>
          <div className={styles.menuContainer}>
            <IconButton
              aria-label="more"
              aria-controls="long-menu"
              aria-haspopup="true"
              onClick={handleClick}
            >
              <MoreVertIcon />
            </IconButton>
            <Menu
              id="long-menu"
              anchorEl={anchorEl}
              keepMounted
              open={open}
              onClose={handleClose}
              PaperProps={{
                style: {
                  maxHeight: ITEM_HEIGHT * 4.5,
                  width: "20ch",
                },
              }}
            >
              {options.map((option: any, index: number) => (
                <MenuItem key={index} onClick={handleClose}>
                  {option}
                </MenuItem>
              ))}
            </Menu>
          </div>
        </div>

        <div className={styles.profileContainer}>
          <div className={styles.profileName}>
            <Typography variant="body1">{userProfile?.user_name}</Typography>
          </div>
          <Button
            variant="contained"
            color="primary"
            size="small"
            onClick={() => signOut()}
            disableElevation
          >
            Sign Out
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Navbar;

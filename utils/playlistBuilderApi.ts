import Cookies from "js-cookie";
import { Itracks, IUser, InewUser } from "./interfaces";

export const verifyUser = async () => {
  const response = await fetch(
    "https://musicplaylistbuilder.azurewebsites.net/api/auth/verify",
    {
      method: "GET",
      headers: {
        Authorization: "Bearer " + Cookies.get("auth-token"),
      },
    }
  );

  if (response.ok) {
    return true;
  }

  return false;
};

export const signIn = async (userEmail: string, userPassword: string) => {
  let user: IUser = {
    email_address: userEmail,
    password: userPassword,
  };

  let userJSON = JSON.stringify(user);

  const response = await fetch(
    "https://musicplaylistbuilder.azurewebsites.net/api/auth/login",
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: userJSON,
    }
  );

  return response;
};

export const registerUser = async (
  userName: string,
  userEmail: string,
  userPassword: string
) => {
  let user: InewUser = {
    user_name: userName,
    email_address: userEmail,
    password: userPassword,
  };

  let userJSON = JSON.stringify(user);

  const response = await fetch(
    "https://musicplaylistbuilder.azurewebsites.net/api/auth/register",

    {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: userJSON,
    }
  );

  return response;
};

export const getUserProfile = async () => {
  const response = await fetch(
    "https://musicplaylistbuilder.azurewebsites.net/api/user/profile",
    {
      headers: {
        Authorization: "Bearer " + Cookies.get("auth-token"),
      },
    }
  );

  const profile = await response.json();

  return profile;
};

export const spotifyLink = async () => {
  const response = await fetch(
    "https://musicplaylistbuilder.azurewebsites.net/api/spotifyauth/link",
    {
      headers: {
        Authorization: "Bearer " + Cookies.get("auth-token"),
      },
    }
  );

  return response;
};

export const spotifyUnLink = async () => {
  const response = await fetch(
    "https://musicplaylistbuilder.azurewebsites.net/api/spotifyauth/unlink",
    {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + Cookies.get("auth-token"),
      },
    }
  );

  return response;
};

export const getPlaylists = async () => {
  const response = await fetch(
    "https://musicplaylistbuilder.azurewebsites.net/api/playlists/all",
    {
      method: "GET",
      headers: {
        Authorization: "Bearer " + Cookies.get("auth-token"),
      },
    }
  );

  const userPlaylists = await response.json();

  return userPlaylists;
};

export const createPlaylist = async () => {
  const response = await fetch(
    "https://musicplaylistbuilder.azurewebsites.net/api/playlists/create",
    {
      method: "POST",
      headers: {
        Authorization: "Bearer " + Cookies.get("auth-token"),
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ playlist_name: "New Playlist" }),
    }
  );
};

export const updatePlaylist = async ({ playlist_id, playlist_name }: any) => {
  const response = await fetch(
    "https://musicplaylistbuilder.azurewebsites.net/api/playlists/update",
    {
      method: "PATCH",
      headers: {
        Authorization: "Bearer " + Cookies.get("auth-token"),
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        playlist_id: playlist_id,
        playlist_name: playlist_name,
      }),
    }
  );
};

export const deletePlaylist = async ({ playlist_id, playlist_name }: any) => {
  const response = await fetch(
    "https://musicplaylistbuilder.azurewebsites.net/api/playlists/delete",
    {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + Cookies.get("auth-token"),
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        playlist_id: playlist_id,
        playlist_name: playlist_name,
      }),
    }
  );
};

export const getPlaylistTracks = async (
  playlist_id: string,
  playlist_name: string
) => {
  const response = await fetch(
    "https://musicplaylistbuilder.azurewebsites.net/api/playlists/tracks",
    {
      method: "POST",
      headers: {
        Authorization: "Bearer " + Cookies.get("auth-token"),
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        playlist_id: playlist_id,
        playlist_name: playlist_name,
      }),
    }
  );

  const tracks = await response.json();

  return tracks;
};

export const deleteTracks = async (tracksToBeDeleted: Array<Itracks>) => {
  const response = await fetch(
    "https://musicplaylistbuilder.azurewebsites.net/api/tracks/bulkdelete",
    {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + Cookies.get("auth-token"),
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(tracksToBeDeleted),
    }
  );

  return response;
};

export const uploadSpotifyPlaylist = async ({ playlist_id }: any) => {
  const response = await fetch(
    "https://musicplaylistbuilder.azurewebsites.net/api/spotify/playlist/create",
    {
      method: "POST",
      headers: {
        Authorization: "Bearer " + Cookies.get("auth-token"),
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        playlist_id: playlist_id,
        spotify_playlist_url: "",
        spotify_track_url: "",
      }),
    }
  );

  return response;
};

export const sendMessage = async ({ subject, reply_email, body }: any) => {
  const response = await fetch(
    "https://musicplaylistbuilder.azurewebsites.net/api/message/send",
    {
      method: "POST",
      headers: {
        Authorization: "Bearer " + Cookies.get("auth-token"),
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        subject: subject,
        reply_email: reply_email,
        body: body,
      }),
    }
  );

  return response;
};

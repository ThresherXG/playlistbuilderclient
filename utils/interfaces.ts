export interface Iplaylist {
  key: string;
  playlist_id: string;
  playlist_name: string;
  spotify_created: boolean;
}

export interface Itracks {
  track_id: string;
  title: string;
  album: string;
  artist: string;
  source: string;
  source_name: string;
  source_url: string;
  spotify_exists: boolean;
  playlist_id: string;
}

export interface IuserProfile {
  user_name: string;
  spotify_display_name: string;
  spotify_link: boolean;
  spotify_account_url: string;
}

export interface IUser {
  email_address: string;
  password: string;
}

export interface InewUser {
  user_name: string;
  email_address: string;
  password: string;
}
